The research follows a general pipeline such as:
* [Input data](#data)
* [Feature engineering](#feature-engineering)
* [Model](#model)
* [Strategy Optimizing](#strategy-optimizing-risk-management)
* [Trading Environment](#trading-environment)
* [Test](#test)

![diagram](diagram1.png)

## Data
We will have to build several database to store two kinds of data, namely financial data and other data. 
* Price data:
Source: Financial data provider
Description: time series data with various of features (OHLC, volume, etc.); 
- Fundamental data
* Non-financial data
	- Macroeconomic data
	Source: World Bank, bloomberg, etc.
	Description: GDP, CPI, etc..
	- Alternative data: GPS. shipping .. 
	- news (sentimental, optional) 
		Source: Google news, bloomberg, financial times, etc.
		Description: Use NLP model to embed  sentences to vectors. The pretrained models are available at tensorflowhub. The output is a 512 float for each embedded sentence or paragraphs. 

### Technical issues
We need to collect data and aggregate the news by each asset. 
- Large storage
- NoSQL database or elasticsearch to store and query data
- Embedding news to vector need a lot of computation power
- Not sure if NLP can have a big improvement to the model.
### Schedule
- Financial data: 
Depends on the data provider. At least one week to indexing the data, get familiar with the scope of the data and also the data structure.
- NLP (optional):
	- Scraping news: At least two weeks, if the hardware and network are ready 
	- Embedding: Need to test on GPU, also depends on the quantity of news
- Macro Economics data: 1.5 week +

## </a>Feature engineering

Depends on the models and the data, we may need to think about feature selection and dimension reduction
- Feature selection;
If we do not include a lot of features and we choose simple models, we can fit the model and remove the insignificant features
- Dimension reduction:
To flatten (embed) the data.
	- PCA
	- self-attention/autoencoder
### Technical issues
Not really
### Schedule
PCA + feature selection: 1 week +
Autoencoder + self-attention: 2 weeks +

## </a>Model
The models may have different types of output:
- Indirect prediction
Easy to apply, to evaluate the accuracy. But need an extra model to generate a action to act on the trading environment 
	- Price prediction : price sequence on a fixed period
		- Linear models (VAR, HAR..)
		- Ensemble models (XgBoost..)
		- RNN models (LSTM..)
		- Transformer model (self-attention model, big success on NLP)
	- Trend prediction : slope on a fixed period
		- image based (optional)
- Direct prediction (reinforcement learning)
End to end solution, but hard to build and train the model. 
	- Portfolio prediction (Multi assets)
	- Next action prediction (Single asset)

### Technical issue
- The direct prediction requires the trading environment to train and evaluate.
- RL model is hard to choose, build and train 
- RL also need a lot of computation power

### Schedule
- Indirect models: 
	-	Can start after having the data
	-	1.5 month +

- Direct models:
	- Can start after having the trading environment
	- 4 month +

## <a id="strategy-optimizing-risk-management)"></a>Strategy Optimizing / Risk management

For the indirect models, after predicting the future price and trend, we need an additional model to choose the action base on the information. It can be a greedy model, or a rule based model considering about various of technical indicators and risk indicators; It can also be another RL model, which is simpler than the one from direct models.
### Schedule
2 weeks + 
## <a id="trading-environment"></a>Trading Environment
Given the time series data, the transaction rules, for each action in a specific timestamp, the env can calculate the return and the next state.

May adapted from Quandopian.

### Schedule
From 1 month to 2 month + depends on if quandopian fits well

## <a id="test"></a>Test
- Single asset test
Test the algorithm at the same asset as it trains from. (train-test split)
- Cross assets test 
Test if the algorithm is generic.


